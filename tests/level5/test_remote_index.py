import os
import pytest
import os
import csv
import shutil
import delune
from delune.fields import *
from rs4 import pathtool, logger
from delune.cli import indexer
import shutil
import time

def index_and_wait (rdir, engine):
    indexer.index (rdir)
    for i in range (3):
        r = engine.get ("/delune/status")
        time.sleep (1)

def test_remote_index (naics, rdir, launch, _index):
    delune.configure (1, logger.screen_logger ()) # for indexer
    confdir = os.path.join (rdir, "delune", "config")
    coldir = os.path.join (rdir, "delune", "collections")

    with launch (os.path.join (os.path.dirname (os.path.dirname (__file__)), "app.py")) as engine:
        dl = delune.mount ("http://localhost:30371/delune")

        r = engine.get ("/test/4")
        assert r.status_code == 200
        assert not r.data ['result']

        col = dl.create ("testcol", ["testcol"])
        col.setopt ("analyzer", make_lower_case = True, stem_level = 1)
        assert os.path.isfile (os.path.join (confdir, "testcol"))
        assert os.path.isdir (confdir)
        assert os.path.isdir (coldir)

        _index (col, naics, 0, count = 10)

        r = engine.get ("/test/4")
        assert r.status_code == 200
        assert len (r.data ['result']) > 1

        assert len (os.listdir (os.path.join (coldir, "testcol", ".que"))) == 1
        print (os.listdir (os.path.join (coldir, "testcol", ".que")))
        col.rollback ()
        assert len (os.listdir (os.path.join (coldir, "testcol", ".que"))) == 1
        print (os.listdir (os.path.join (coldir, "testcol", ".que")))

        indexer.index (rdir)
        _index (col, naics, 500)
        col.commit ()
        assert len (os.listdir (os.path.join (coldir, "testcol", ".que"))) >= 6

        indexer.index (rdir)
        assert len (os.listdir (os.path.join (coldir, "testcol", ".que"))) == 0
        col.close ()

        r = engine.get ("/delune/status"); time.sleep (2)
        assert r.status_code == 200

        col = dl.load ("testcol")
        with col.documents as ds:
            r = ds.search ("service")
            assert "id" in r ["result"][0][0]
            assert r ['total'] == 225

            r = ds.search ("service", nth_content = 1)
            assert "full" in r ["result"][0][0]
            assert "<b>" in r ["result"][0][1]

            r = ds.search ("service",  partial = "code")
            assert r ["code"] == 500

            r = ds.search ("service",  partial = "id")
            assert len (r ["result"][0][0]) == 1
            assert "<b>" in r ["result"][0][1]

        _index (col, naics, 500)
        _index (col, naics, 500)

        col.commit ()
        index_and_wait (rdir, engine)

        with col.documents as ds:
            r = ds.search ("service")
            assert r ['total'] == 225

            ds.qdelete ("service")
            col.commit ()
            index_and_wait (rdir, engine)

            r = ds.search ("service")
            assert r ['total'] == 0

            r = ds.search ("equipment")
            assert r ['total'] == 111

            ds.truncate ("testcol")
            col.commit ()
            index_and_wait (rdir, engine)

            r = ds.search ("equipment")
            assert r ['total'] == 0

            r = ds.search ("service")
            assert r ['total'] == 0

        _index (col, naics, 500)
        col.commit ()
        index_and_wait (rdir, engine)

        with col.documents as ds:
            r = ds.search ("service")
            assert r ['total'] == 225

            r = ds.get ("812320")
            assert r ['total'] == 1

            ds.delete ("812320")
            col.commit ()
            index_and_wait (rdir, engine)

            r = ds.get ("812320")
            assert r ['total'] == 0

        _index (col, naics, 500, False)
        col.commit ()
        index_and_wait (rdir, engine)

        with col.documents as ds:
            r = ds.search ("service")
            assert r ['total'] == 449

            ds.qdelete ("service")
            col.commit ()
            index_and_wait (rdir, engine)

            r = ds.search ("service")
            assert r ['total'] == 0

        col.close ()
        col.drop (True)
        assert not os.path.isdir (os.path.join (confdir, "testcol"))

        r = engine.get ("/test/4")
        assert r.status_code == 200
        print (r.data)
        assert len (r.data ['result']) > 20
